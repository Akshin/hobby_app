export default {
    addHobbyToSelf(state, hobby) {
        state.SELF_HOBBY_LIST.unshift(hobby);
    },
    removeHobby(state, idx) {
        state.SELF_HOBBY_LIST.splice(idx, 1);
    }
}