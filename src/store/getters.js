function stringsAreEqual(first, second) {
    const f = first.toLowerCase();
    const s = second.toLowerCase();
    return f === s;
}

export default {
    hobbyExistsInSelf: state => name => {
        return state.SELF_HOBBY_LIST.find(hobby => stringsAreEqual(hobby, name));
    }
}