export default {
    SELF_HOBBY_LIST: [
        'Хоккей',
        'Высокоточная вёрстка под старые версии Microsoft Internet Explorer, начиная с версии 5.01',
        'Музыка',
        'Stand up'
    ],
    FRIEND_HOBBY_LIST: [
        'Баскетбол',
        'Нарезка Photoshop/Fireworks макетов на скорость, в экстримельных условиях, на природе',
        'Тренировки',
    ]
}