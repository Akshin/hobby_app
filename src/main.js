import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store";

import InputText from "./components/inputs/InputText.vue"

Vue.component('input-text', InputText)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
